//Model for grocery_list table
//Sequelize.STRING ~ VARCHAR(255)
//Sequelize.INTEGER ~ INTEGER
//Sequelize.BIGINT(12)~ BIGINT(12)

module.exports = function (sequelize, Sequelize) {
    var Grocery = sequelize.define("grocery_list",
        {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                autoIncrement:true
            },
            upc12: {
                type: Sequelize.BIGINT(12),
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            tableName: 'grocery_list',
            timestamps: false
         });
    console.log("model groceryList ",Grocery);
    return Grocery;
};
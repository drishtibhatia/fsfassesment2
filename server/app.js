var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 8080;

const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'krishna1234';

var app = express();

// Creates a MySQL connection
var sequelize = new Sequelize(
    'grocery',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {   // default port    : 3306
        host: 'localhost',         
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);
//sequelize.authenticate();

// Loads model for Grocery table
var Grocery = require('./models/grocery')(sequelize, Sequelize);

app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());


//retrive first 20 products from database 
app.get("/api/groceryList", function(req, res){
    console.log(req.query.searchString);
    var searchString  = req.query.searchString;
    //res.status(200).send("its in server");
    if(searchString){
        Grocery
            .findAll({
                where: { 
                    $or: [
                        {brand: {$like: "%" + req.query.searchString + "%"}},
                        {name: {$like: "%" + req.query.searchString + "%"}}
                    ]
                },limit: 20
            }).then(function(groceryList){
                res
                    .status(200)
                    .json(groceryList);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    }else{
        
     Grocery
        .findAll({ limit:20
        }).then(function(groceryList){
            res
                .status(200)
                .json(groceryList);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })   
    }   
    
});

// -- Searches for specific grocery item by id 
app.get("/api/groceryList/:id", function (req, res) {
    console.log("inside server function....",req.params.id);
    Grocery
        .find({
            where: { id : req.params.id}
        })
        .then(function (results) {
            console.log("result:  " + JSON.stringify(results));
            res
                .json(results);
        })
        .catch(function (err) {
            console.log("result:  " + JSON.stringify(err));
            res
                .status(500)
                .json({error: true});
        });
});

app.put('/api/groceryList/save/:id', function (req, res) {
    console.log("inside PUT sever ....");
    console.log("body:", req.body);
    console.log("params:", req.params);

    Grocery
        .update(
            { upc12: req.body.upc,
              brand: req.body.brand,
              name: req.body.name  
             },   
             { where:{ id : req.params.id } }  
        )
        .then(function (response) {
              res
                .status(200)
                .send(response);
        })
        .catch(function (err) {
             res
                .status(500)
                .json({error: true});
        });
});

// Error handler 
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});
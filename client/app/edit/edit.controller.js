(function () {
    'use strict';
    angular
        .module("GMS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["Service",'$stateParams',"$state"];

  function EditCtrl(Service, $stateParams, $state) {
        var vm = this;
        //console.log($stateParams);
        //console.log($stateParams.id);
        var id = $stateParams.id;
        console.log(id);
        vm.product = id;
        vm.item = {};
        
        vm.cancel = function () {
            $state.go("list");
        };
        
        Service.edit($stateParams.id)
            .then(function (item) {
                console.log("edting the item as this :",item.data);
                vm.item = item.data;
            }).catch(function (err) {
                console.info("Some Error Occured",err)
            });

        vm.save = function () {
            console.log("Saving the changes to database: "
            ,vm.item.id , vm.item.upc12 , vm.item.brand,vm.item.name);
            Service.save(vm.item.id,
                        vm.item.upc12,
                        vm.item.brand,
                        vm.item.name)
                .then(function (response) {
                    console.info("item saved in database....",response);
                    $state.go("list");
                }).catch(function (err) {
                console.info("Some Error Occured",err)
            });
        } //end of save()

    }//end of controller 
   
})();




(function () {

    angular
        .module("GMS")
        .service("Service", Service);

    Service.$inject = ['$http'];

    function Service($http) {

        var service = this;

        // Exposed data models 
        // Exposed functions 
        
        service.getGroceryList = getGroceryList;
        service.edit = edit;
        service.save = save;

        //get full grocery list based on search string : empty or searchString
        function getGroceryList(searchString) {
            console.log("from controller with search query to service ");
            return $http({
                method: 'GET'
                , url: 'api/groceryList'
                , params: {
                    'searchString': searchString
                }
            });
        }

        //get single record based on requested ID 
        function edit(id){
            console.log("Service to get requested item for edit");
             return $http({
                method: 'GET'
                , url: "api/groceryList/" + id
            });
        }
         
         //send updates req to sever 
        function save(id,upc,brand,name) {
            console.log('id: '+id+
                        ' upc: '+upc+
                        ' brand: '+brand+
                        ' name: '+name);
            return $http({
                method: 'PUT'
                , url: 'api/groceryList/save/' + id
                , data: {
                    id:id,
                    upc:upc,
                    brand:brand,
                    name:name
                }
            });
        }
    
    }//end of service function 
})();
(function () {
    angular.module("GMS")
        .config(GMSconfig);
    
    GMSconfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function GMSconfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("list", {
                url: "/list",
                templateUrl: "/app/list/list.html",
                controller: "ListCtrl as ctrl"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/app/edit/edit.html",
                controller: "EditCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/list");
    }

})();

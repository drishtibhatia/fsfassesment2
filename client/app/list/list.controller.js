
(function (){
     angular
        .module("GMS")
        .controller("ListCtrl", ListCtrl);

    ListCtrl.$inject = ['Service','$state','$scope'];

   function ListCtrl(Service,$state,$scope){
        var vm = this;
        
        vm.searchString = '';
        vm.result = null;
        vm.search = search;
        vm.getItem = getItem;
       
        $scope.propertyName = 'name';
        $scope.reverse = false;
        $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        };

        init();

        function init(){
            retrieveGroceryList('');
        }

        function search(){
            retrieveGroceryList(vm.searchString);
        }

        function retrieveGroceryList(param){
            Service
                .getGroceryList(param)
                .then(function (results){
                    vm.groceryList = results.data;
                })
                .catch(function (err){
                    console.log("error " + err);
                });
        }

        function getItem(id) {
            $state.go("edit", {'id' : id});
        };

   }//end of controller function 

})();